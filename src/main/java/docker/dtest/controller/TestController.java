package docker.dtest.controller;

import docker.dtest.annotation.MethodAnnotation;
import docker.dtest.domain.SlackWebHook;
import docker.dtest.util.Base64Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author Goddy
 * @Date Create in 下午2:22 2018/4/6
 */
@RestController
@Slf4j
public class TestController {

    @Value("${slack.url}")
    private String SlackUrl;

    @Value("${CONSTANT.version}")
    private String version;

    @GetMapping("/")
    public Object defaultAPI() {
        log.debug("【log test】debug");
        log.info("【log test】info");
        log.warn("【log test】warn");
        log.error("【log error】error");
        return "Version is : " + version;
    }

    /**
     * 测试接口
     * @return
     */
    @GetMapping(value = "/testSlack")
    public Object test() {

        SlackWebHook slackWebHook = new SlackWebHook("本地测试", SlackUrl);
        slackWebHook.addField("测试内容1", "执行成功");
        slackWebHook.addField("测试内容2", "执行失败");
        slackWebHook.send();
        return "finish!";
    }

    /**
     * 登陆
     * @param name
     * @return
     */
    @MethodAnnotation("wcm520")
    @RequestMapping(value = "/name/{name}")
    public Object name(@PathVariable String name) {
        log.info("tesssssssst");
        return "Welcome, " + name + "!";
    }

    /**
     * 测试RESTful api
     * @param name
     * @param uid
     * @return
     */
    @RequestMapping(value = "/test/{name}/uid/{uid}")
    public Object name(@PathVariable String name, @PathVariable String uid) {

        return name + "<br>" + uid;
    }

    /**
     * 生成加密秘钥
     * @return
     */
    @RequestMapping(value = "/encode")
    public Object encode() {
        return Base64Util.encode("wcm/i\\love,u heart");
    }

    /**
     * 解密
     * @param value
     * @return
     */
    @RequestMapping(value = "/decode/{value}")
    public Object decode(@PathVariable String value) {
        return Base64Util.decode(value);
    }
}
