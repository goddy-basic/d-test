package docker.dtest.controller;

import docker.dtest.annotation.DecodeAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Goddy
 * @Date Create in 上午9:40 2018/4/17
 */
@Slf4j
@RestController
@RequestMapping(value = "/name/{name}/")
public class TestAnnotationController {

    /**
     * 自定义注解自动decode
     * @param name
     * @param uid
     * @return
     */
    @RequestMapping(value = "/uid/{uid}")
    public Object uid(@PathVariable String name, @DecodeAnnotation String uid) {
        return name + "<br>" + uid;
    }
}
