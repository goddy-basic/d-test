package docker.dtest.config;

import docker.dtest.resolver.DecodeResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

/**
 * @Author Goddy
 * @Date Create in 下午3:01 2018/4/17
 */
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurationSupport {

    @Override
    protected void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {

        super.addArgumentResolvers(argumentResolvers);

        //注册decode的参数处理器
        argumentResolvers.add(new DecodeResolver());
    }
}
