package docker.dtest.domain;

import docker.dtest.util.ColorUtil;
import docker.dtest.util.HttpRequestUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

import java.util.ArrayList;

/**
 * @Author Goddy
 * @Date Create in 下午4:55 2018/4/19
 */
@Slf4j
public class SlackWebHook {

    private String url;

    private JSONObject slack;

    private ArrayList<JSONObject> fields = new ArrayList<>();

    public SlackWebHook(JSONObject slack, String webHookUrl) {
        this.url = webHookUrl;
        this.slack = slack;
    }

    /**
     * 构造器
     * @param text
     */
    public SlackWebHook(String text, String webHookUrl) {
        this.url = webHookUrl;
        slack = new JSONObject();
        slack.put("fallback", "");
        slack.put("text", text);
        slack.put("color", ColorUtil.getRandomColor());
        slack.put("fields", fields);
    }

    /**
     * 添加一条field
     * @param title 名称
     * @param value 内容
     */
    public void addField(String title, String value) {
        System.out.println(slack);
        JSONObject temp = new JSONObject();
        temp.put("title", title);
        temp.put("value", value);
        temp.put("short", false);
        fields.add(temp);
        slack.put("fields", fields);
    }

    public void send() {
        log.info(url);
        HttpRequestUtil.sendPostJson(url, slack.toString());
    }
}
