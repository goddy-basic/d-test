# d-test

此示例项目包含：
* 一、Slf4j 简单配置
* 二、Slf4j 复杂配置
* 三、java 打包
* 四、base64 工具类
* 五、自定义方法注释、拦截器
* 六、自定义参数注释、处理器
* 七、slack webhook 工具类
* 八、javadoc 输出文件
* 九、根据环境切换变量
* 十、banner 配置
* 十一、打包文件命名

*注：项目名称跟内容无关。*

## 一、Slf4j 简单配置
```
logging:
  pattern:
    console: "%d - [%level]%msg%n"
  #默认放置该文件夹下新建spring.log
  path: ./log/
  level:
    docker.dtest: info
```

## 二、Slf4j 复杂配置
在resources文件下新建logback-spring.xml
*注：启用需要注释掉配置文件中log的配置*
```
<?xml version="1.0" encoding="UTF-8" ?>
<configuration>

    <!--配置项-->
    <appender name="consoleLog" class="ch.qos.logback.core.ConsoleAppender">
        <layout class="ch.qos.logback.classic.PatternLayout">
            <pattern>
                %d - [%level]%msg%n
            </pattern>
        </layout>
    </appender>

    <!--配置文件-->
    <appender name="fileInfoLog" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>ERROR</level>
            <onMatch>DENY</onMatch>
            <onMismatch>ACCEPT</onMismatch>
        </filter>
        <encoder>
            <pattern>
                %d - [%level]%msg%n
            </pattern>
        </encoder>
        <!--滚动策略-->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!--路径-->
            <fileNamePattern>
                log/test/info.%d.log
            </fileNamePattern>
        </rollingPolicy>
    </appender>

    <appender name="fileErrorLog" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
            <level>ERROR</level>
        </filter>
        <encoder>
            <pattern>
                %d - [%level]%msg%n
            </pattern>
        </encoder>
        <!--滚动策略-->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!--路径-->
            <fileNamePattern>
                log/test/error.%d.log
            </fileNamePattern>
        </rollingPolicy>
    </appender>

    <!--用到哪-->
    <root level="info">
        <appender-ref ref="consoleLog" />
        <appender-ref ref="fileInfoLog" />
        <appender-ref ref="fileErrorLog" />

    </root>

</configuration>
```

## 三、java 打包
```
# 打包
$ mvn clean package

# 跳过测试步骤
$ mvn clean package -Dmaven.test.skip=true
```

## 四、base64 工具类 五、自定义方法注释、拦截器 六、自定义参数注释、处理器五、六
详见：
* 简书：https://www.jianshu.com/p/19777576fde5
* 代码地址：https://gitlab.com/goddy-basic/basic_annotation

## 七、slack webhook 工具类
详见代码：
* HttpRequestUtil 
* ColorUtil
* SlackWebHook
* TestController

## 八、javadoc 输出文件
//TODO: Goddy 2018/04/019 [待填充内容]

## 九、根据环境切换变量
1、配置文件设置
```
# create dev/test/prod environment
$ touch application-dev.yml
$ touch application-test.yml
$ touch application-prod.yml

# open application.yml
spring:
  profiles:
    active: dev
```
2、启动命令配置
```
# Ctrl + C 或直接关闭窗口 打断程序
$ java -jar docker-test.jar --spring.profiles.active=prod

# 窗口关闭 终止程序
$ java -jar docker-test.jar --spring.profiles.active=prod &

# 根据端口占用，杀掉进程来终止程序
$ nohup java -jar docker-test.jar --spring.profiles.active=prod &

# 附：nohup 查看日志
$ tail -fn 10 nohup.out
```

## 十、banner 配置
取名为banner.txt，放到resources文件夹下即可。

## 十一、打包文件命名
```
# open pom.xml
# 添加finalName，内容为打包的名称
<build>
    <finalName>docker-test</finalName>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```